const utils = 
{
    data(){
        return{
            projects: null
        }
    },
    getProjects(onSuccess)
    {
        /*if (this.projects != null)
        {
            onSuccess(this.projects);
            return;
        }*/

        const path = "./../data/projects.json";
        /*const path = "http://annahofseth.com/rs/data/projects.json";
        const conversionUrl = 'https://cors-anywhere.herokuapp.com/';*/

        fetch(path, {
          mode : "cors",
          method : "GET",
          headers : {
              'Content-Type' : 'application/json'
          }})
        .then(function(response) {
            return response.json();
        })
        .then(function(result) {
            this.projects = result.projects;
            onSuccess(result.projects);
        });
    }
}