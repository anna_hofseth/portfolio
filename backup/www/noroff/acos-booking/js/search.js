function loadJson ()
{
	var link = "http://www.annahofseth.com/noroff/workflow1/acos-booking/resources/json/bookings.json";
	let obj;

	fetch(link)
	.then(result => result.json())
	.then((res) => obj = res)
	.catch(err => console.log(err))

	return JSON.parse(obj);
}

var categoryList = ["vigsel", "saksbehandler", "idrettsanlegg", "grupperom", "kjøretøy", "selskapslokale", "konferanserom", "aktiviteter"];

function setSearch ()
{
	if (typeof(Storage) !== "undefined")
		localStorage.setItem("booking_searchInput", document.getElementById("searchField").value);

	else console.error("This browser does not support Web Storage");
}

function setCategory (category)
{
	if (typeof(Storage) !== "undefined")
		localStorage.setItem("booking_category", category);

	else console.error("This browser does not support Web Storage");
}

function loadSearchPage ()
{
	console.log("loadSearchPage()");

	var category = "";
	var searchInput = "";
	if (typeof(Storage) !== "undefined")
	{
		category = localStorage.getItem("booking_category");
		searchInput = localStorage.getItem("booking_searchInput");

		if (category == "") category = null;
		if (searchInput == "") searchInput = null;

		if (category == null && searchInput == null)
		{
			document.getElementById("searchResults").innerHTML = "No search results";
		}

		else
		{
			if (searchInput != null)
			{
				document.getElementById("searchField").value = searchInput;
			}

			else
				document.getElementById("searchField").value = "";

			if (category != null)
			{
				let catIndex = 0;
				for (let cI = 0; cI < categoryList.length; cI++)
					if (category == categoryList[cI])
					{
						catIndex = cI;
						cI = categoryList.length;
					}

				document.getElementById("categoryField").selectedIndex = catIndex + 1;
			}

			else
				document.getElementById("categoryField").selectedIndex = 0;

			displaySearchResults();
		}

		localStorage.removeItem("booking_category");
		localStorage.removeItem("booking_searchInput");
	}

	else console.error("This browser does not support Web Storage");
}

function displaySearchResults ()
{
	var jsonObj = JSON.parse(hardJson);

	var inputText = document.getElementById("searchField").value;
	var inputCategory = document.getElementById("categoryField").value;

	var resultContent = "";

	if (inputCategory.toLowerCase() != "kategori")
	{
		var jsonObj = JSON.parse(hardJson);

			console.log(inputCategory);

		if (inputCategory.toLowerCase() == "vigsel")
			jsonObj = jsonObj.vigsel;
		if (inputCategory.toLowerCase() == "selskapslokale")
			jsonObj = jsonObj.selskapslokale;
		if (inputCategory.toLowerCase() == "idrettsanlegg")
			jsonObj = jsonObj.idrettsanlegg;
		if (inputCategory.toLowerCase() == "saksbehandler")
			jsonObj = jsonObj.saksbehandler;
		if (inputCategory.toLowerCase() == "konferanserom")
			jsonObj = jsonObj.konferanserom;
		if (inputCategory.toLowerCase() == "grupperom")
			jsonObj = jsonObj.grupperom;
		if (inputCategory.toLowerCase() == "kjøretøy")
			jsonObj = jsonObj.kjøretøy;
		if (inputCategory.toLowerCase() == "aktiviteter")
			jsonObj = jsonObj.aktiviteter;
		else
			console.log(inputCategory.toLowerCase(),"fins ikkje?");

		resultContent += getCategoryDivs(jsonObj, inputText);
	}

	else
	{
		resultContent += getCategoryDivs(jsonObj.vigsel, inputText);
		resultContent += getCategoryDivs(jsonObj.selskapslokale, inputText);
		resultContent += getCategoryDivs(jsonObj.idrettsanlegg, inputText);
		resultContent += getCategoryDivs(jsonObj.saksbehandler, inputText);
		resultContent += getCategoryDivs(jsonObj.konferanserom, inputText);
		resultContent += getCategoryDivs(jsonObj.grupperom, inputText);
		resultContent += getCategoryDivs(jsonObj.kjøretøy, inputText);
		resultContent += getCategoryDivs(jsonObj.aktiviteter, inputText);
	}

	document.getElementById("searchResults").innerHTML = resultContent;
}

function getCategoryDivs (categoryObj, text)
{

	var result = "";

	console.log(categoryObj);
	for (let i = 0; i < categoryObj.length; i++)
	{
		if (text != null)
		{
			if (categoryObj[i].navn.toLowerCase().search(text.toLowerCase()) != -1)
			{
				result += getResultDiv(categoryObj[i]);
			}
		}

		else
			result += getResultDiv(categoryObj[i]);
	}

	return result;
}

function getResultDiv (categoryItemObj)
{
	return `
			<a href="location.html" class="location">
				<img class="locationimg" src="../resources/images/room1.jpg" alt="location">
				<h2 class="locationname">`+categoryItemObj.navn+`</h2>
				<p class="locationinfo">`+categoryItemObj.beskrivelse+`</p>
				<p class="locationprice">`+categoryItemObj.pris+` kr/t</p>
			</a>
	`;
}

var hardJson = `{
	"vigsel": [
		{
			"navn": "Vigsel sted 1",
			"sted": "Sted",
			"pris": "400",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Vigsel sted 2",
			"sted": "Sted",
			"pris": "1000",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Vigsel sted 3",
			"sted": "Sted",
			"pris": "12",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Vigsel sted 4",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}
	],

	"selskapslokale": [
		{
			"navn": "Selskapslokale sted 1",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Selskapslokale sted 2",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Selskapslokale sted 3",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Selskapslokale sted 4",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}
	],

	"idrettsanlegg": [
		{
			"navn": "Idrettsanlegg sted 1",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Idrettsanlegg sted 2",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Idrettsanlegg sted 3",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Idrettsanlegg sted 4",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}
	],

	"saksbehandler": [
		{
			"navn": "Saksbehandler 1",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Saksbehandler 2",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Saksbehandler 3",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Saksbehandler 4",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}
	],

	"konferanserom": [
		{
			"navn": "Konferanserom sted 1",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Konferanserom sted 2",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Konferanserom sted 3",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Konferanserom sted 4",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}
	],

	"grupperom": [
		{
			"navn": "Grupperom sted 1",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Grupperom sted 2",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Grupperom sted 3",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Grupperom sted 4",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}
	],

	"kjøretøy": [
		{
			"navn": "Kjøretøy 1",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Kjøretøy 2",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Kjøretøy 3",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Kjøretøy 4",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}
	],

	"aktiviteter": [
		{
			"navn": "Aktivitet 1",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Aktivitet 2",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Aktivitet 3",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}, 
		
		{
			"navn": "Aktivitet 4",
			"sted": "Sted",
			"pris": "0",
			"beskrivelse": "Kort beskrivelse"
		}
	]
}`;