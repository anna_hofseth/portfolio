function loadReservedBooking ()
{
	console.log("loadReservedBooking");
}

function validateFirstName (valueChange = false)
{
	if (valueChange){
		document.getElementById("firstNameError").style.display = "none";
		return;
	}

	var valid = false;
	var firstName = document.getElementById("firstName");
	if (firstName.value.length > 0) valid = true;

	if (!valid) document.getElementById("firstNameError").style.display = "block";
	if (valid) document.getElementById("firstNameError").style.display = "none";

	return valid;
}

function validateLastName (valueChange = false)
{
	if (valueChange){
		document.getElementById("lastNameError").style.display = "none";
		return;
	}
	
	var valid = false;
	var lastName = document.getElementById("lastName");
	if (lastName.value.length > 0) valid = true;

	if (!valid) document.getElementById("lastNameError").style.display = "block";
	if (valid) document.getElementById("lastNameError").style.display = "none";

	return valid;
}

function validatePhone (valueChange = false)
{
	if (valueChange){
		document.getElementById("phoneError").style.display = "none";
		return;
	}
	
	var valid = false;
	var phone = document.getElementById("phone").value;
	phone = phone.split(" ").join("");
	phone = phone.split(".").join("");
	phone = phone.split("-").join("");
	phone = phone.trim();

	if (/^[0-9]{8,10}$/.test(phone))
		valid = true;

	if (!valid) document.getElementById("phoneError").style.display = "block";
	if (valid) document.getElementById("phoneError").style.display = "none";

	return valid;
}

function validateMail (valueChange = false)
{
	if (valueChange){
		document.getElementById("emailError").style.display = "none";
		return;
	}
	
	var valid = false;
	var email = document.getElementById("email").value;
	if (/^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]{2,4}$/.test(email))
		valid = true;

	if (!valid) document.getElementById("emailError").style.display = "block";
	if (valid) document.getElementById("emailError").style.display = "none";

	return valid;
}

function validateAll ()
{
	var valid = false;

	if (validateFirstName() && validateLastName() && validatePhone() && validateMail())
		valid = true;

	return valid;
}

function submitClick()
{
	var valid = validateAll();
}