// The ids of the houses used
var houseIds = ["362", "378", "229", "15", "16", "34", "169"];
{
	// Stark: 362
	// Targaryen: 378
	// Lannister: 229
	// Baratheon: 15
	// Bolton: 34
	// Greyjoy: 169
	// Tyrell of Highdarden: 398

}
// The ids of all playable characters
var characterIds = ["583", "1303", "339", "1052", "238", "148", "1963", "901", "150", "1022"];
{
	// John: 583
	// Deanerys: 1303
	// Grey Worm: 1445
	// Cercei: 238
	// Robert: 901
	// Tywin: 27
	// Jamie: 529
	// Tyrion: 1052
	// Sansa: 957
	// Arya: 148
	// Ned: 339
	// Stannis: 1963
	// Missandrei: 1709
	// Jorah: 1560
	// Theon: 1022
	// Yara/Asha: 150
	// Ramsay: 849
	// Davos: 1319
	// Drogo: 1346
	// Sam: 954
	// Ygritte: 2126
	// Tormund: 2024
	// The mountain: 1442
	// The hound: 955
	// Jaqen H'ghar: 1532
	// Brienne: 216
}

var houses = [];
var characters = [];

var cards = [];
var selectedCards = [];

//localStorage.clear();
loadHouses(loadCharacters);

/*function searchEngine(type, search)
{
	for(let i = 1; i < 50; i++)
		downloadJson("https://www.anapioficeandfire.com/api/"+type+"?page="+i+"&pageSize=50", function(jsonObj)
		{
			for(let obj of jsonObj)
			{
				if (JSON.stringify(obj).search(search));
					console.log(obj.name, obj.url);
			}
		});
}*/

// Loading selection of houses.
// This is done before loading the characters, because the depend on their houses
function loadHouses()
{
	for(let houseId of houseIds)
	{
		// Checking local storage before downloading
		if (localStorage.getItem("iaf-house-" + houseId))
		{
			let house = JSON.parse(localStorage.getItem("iaf-house-" + houseId));
			setHouse(house);
		}
		
		else
		{
			let housePath = "https://anapioficeandfire.com/api/houses/";
			downloadJson((housePath + houseId), setHouse);
		}
	}
}

function setHouse(house)
{
	let houseId = urlToId(house.url);
	houses.push(house);

	// Saving object to local storage
	if (!localStorage.getItem("iaf-house-" + houseId))
		localStorage.setItem("iaf-house-" + houseId, JSON.stringify(house));

	// Loading characters once all houses are loaded
	if (houses.length == houseIds.length)
	{
		loadCharacters();
	}
}

// Loading selection of characters
function loadCharacters()
{
	let characterPath = "https://anapioficeandfire.com/api/characters/";

	for(let id of characterIds)
	{
		if (localStorage.getItem("iaf-character-" + id))
		{
			let character = JSON.parse(localStorage.getItem("iaf-character-" + id));
			setCharacter(character);
		}

		else
			downloadJson((characterPath + id), setCharacter);
	}
}

function setCharacter(character)
{
	characters.push(character);
	let characterId = urlToId(character.url);

	// Saving object to local storage
	if (!localStorage.getItem("iaf-character-" + characterId))
		localStorage.setItem("iaf-character-" + characterId, JSON.stringify(character));

	let house = character.allegiances.length > 0 ? character.allegiances[0] : undefined;

	// Some characters is missing their allegiances
	if (house == undefined)
	{
		if (characterId == "901") house = "16"; // Robert Baratheon: House Baratheon (Dragonstone)
		if (characterId == "1963") house = "15"; // Stannis Baratheon: House Baratheon (Kings' Landing)
	}

	let newCard = new Card(character, houseUrlToObject(house));
	cards.push(newCard);

	// Drawing the rest of the page once all data has been loaded
	if (characters.length == characterIds.length)
	{
		drawCards();
	}
}

// Todo: remove data-card-index, replace with id?
function drawCards()
{
	let cardsRoot = document.getElementById("cardsRoot");
	cardsRoot.innerHTML = "";

	for(let card of cards)
	{
		let cardElement = getCardElement(card);
		cardsRoot.appendChild(cardElement);
	}
}

// Called when clicking on a card. Only used in the index-page
// Validating if two cards are selected
function onCardClick(element)
{
	let clickedCard;

	for(let card of cards)
	{
		if (element.dataset.id == card.characterId)
			clickedCard = card;
	}

	if (element.dataset.selected == 1)
	{
		for (let i = 0; i < selectedCards.length; i++)
			if (clickedCard == selectedCards[i])
			{
				selectedCards.splice(i, 1);
				element.dataset.selected = 0;
				element.classList.remove("selected");
			}

	}

	else
	{
		if (selectedCards.length < 2)
		{
			selectedCards.push(clickedCard);
			element.dataset.selected = 1;
				element.classList.add("selected");
		}
	}

	if (selectedCards.length == 2)
	{
		document.getElementById("startButton").style.display = "block";
		document.getElementById("selectChampionText").style.display = "none";

		localStorage.setItem("iaf-champions", 
		`
		{
			"champions": [
			{
				"characterId": `+selectedCards[0].characterId+`, 
				"houseId": `+selectedCards[0].houseId+`
			},
			{
				"characterId": `+selectedCards[1].characterId+`, 
				"houseId": `+selectedCards[1].houseId+`
			}]
		}
		`);
	}

	else
	{
		document.getElementById("startButton").style.display = "none";
		document.getElementById("selectChampionText").style.display = "block";
	}
}


