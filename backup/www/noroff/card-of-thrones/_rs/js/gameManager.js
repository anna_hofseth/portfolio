var tilesCanvas;
var tilesCanvasElement = document.getElementById("tilesCanvas");
var playersCanvas;
var playersCanvasElement = document.getElementById("playersCanvas")

var diceCanvas;
var diceCanvasElement = document.getElementById("diceCanvas");
var diceTurn = document.getElementById("diceTurn");

var mapImage = document.getElementById("mapBackgroundImage");
var popupElement = document.getElementById("popupContainer");

var mapImageSize = {x: 1920, y: 1080};

// Map-step coordinates in pixel values, relative to the map image source
var mapSteps = [
	{x: 1660, y: 460},
	{x: 1640, y: 520},
	{x: 1580, y: 560},
	{x: 1500, y: 540, trap:
		{name: " is attacked by a Dothraki horde! Move 2 steps back",
		 steps: -2}},
	{x: 1420, y: 500},
	{x: 1330, y: 500},
	{x: 1260, y: 520},
	{x: 1220, y: 570, trap:
		{name: " is attacked by a large dothraki horde! Move 3 steps back",
		 steps: -3}},
	{x: 1240, y: 640},
	{x: 1300, y: 700},
	{x: 1280, y: 780},
	{x: 1200, y: 820},
	{x: 1120, y: 800},
	{x: 1040, y: 760},
	{x: 960, y: 760},
	{x: 910, y: 810},
	{x: 840, y: 840},
	{x: 760, y: 800},
	{x: 680, y: 820, trap:
		{name: " is attacked by ironborn pirates! Move 3 steps back",
		 steps: -3}},
	{x: 620, y: 860},
	{x: 540, y: 880},
	{x: 460, y: 870},
	{x: 390, y: 830},
	{x: 380, y: 760,trap:
		{name: " is attacked by outlaws! Move 2 steps back",
		 steps: -2}},
	{x: 380, y: 680},
	{x: 320, y: 640},
	{x: 300, y: 570},
	{x: 340, y: 520},
	{x: 420, y: 500},
	{x: 500, y: 540, trap:
		{name: " is attacked by the kingsguard! Move 4 steps back",
		 steps: -4}},
	{x: 530, y: 600} // red keep
];

var cards = [];
var currentCardTurnIndex = 0;
var currentCard;

var lastDiceRoll = 0;
var diceRollReady = false;
var canGetBonusTurn = true;

if (!tilesCanvasElement.getContext)
	alert(`Your browser does not support HTML Canvas. 
		The game will not be playable. Please use another web browser.`);

tilesCanvas = document.getElementById("tilesCanvas").getContext("2d");
playersCanvas = document.getElementById("playersCanvas").getContext("2d");
diceCanvas = document.getElementById("diceCanvas").getContext("2d");
initCanvas(diceCanvasElement);

initGame();

// Initializing the game, loading api-objects from local storage
function initGame()
{
	if (!localStorage.getItem("iaf-champions"))
	{
		alert("No characters are selected.");
		return;
	}

	let selectedChampions = JSON.parse(localStorage.getItem("iaf-champions")).champions;
	let championsRoot = document.getElementById("championsDeck");
	let diceElement = document.getElementById("diceCanvas").parentElement;
	diceElement.setAttribute("onclick", "onDiceClick();");

	let viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	let viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
	let cardElementWidth = 360;
	let cardElementHeight = 556;
	let championsRootTotalSpacing = 20;

	let cardElementAvailableHeight = viewportHeight * (2/5) - championsRootTotalSpacing;
	let cardElementScale = cardElementAvailableHeight / cardElementHeight;
	let championsDeckNewWidth = cardElementWidth * cardElementScale;

	championsRoot.style.width = championsDeckNewWidth;

	let index = 0;
	for(let champ of selectedChampions)
	{
		let character = JSON.parse(localStorage.getItem("iaf-character-"+champ.characterId));
		let house = JSON.parse(localStorage.getItem("iaf-house-"+champ.houseId));
		let card = new Card(character, house);
		cards.push(card);

		let cardElement = getCardElement(card, false);
		let cardElementContainer = document.createElement("div");
		cardElementContainer.classList.add("cardContainer");
		cardElementContainer.appendChild(cardElement);
		
		championsRoot.appendChild(cardElementContainer);

		// Placing the dice rolling space in between the cards
		if (index == 0)
			championsRoot.insertBefore(diceElement, cardElementContainer);

		cardElement.style.transform = "scale("+cardElementScale+", "+cardElementScale+")";
		cardElement.style.transformOrigin = "0% 0%";
		cardElementContainer.style.width = cardElementWidth * cardElementScale;
		cardElementContainer.style.height = cardElementHeight * cardElementScale;

		cardElementContainer.style = `
			width: `+cardElementWidth * cardElementScale+`px;
			height: `+cardElementHeight * cardElementScale+`px;
		`;

		card.position = mapSteps[0];
	}

	document.getElementById("mapBackgroundImage").onload = initMap;
}

// Setting element sizes and other initial map settings
function initMap()
{
	let mapContainerElement = document.getElementById("mapContainer");
	currentCard = cards[currentCardTurnIndex];

	for(let el of 
		[mapContainerElement, 
		tilesCanvasElement, 
		playersCanvasElement])
	{
		el.style.width = mapImage.width + "px";
		el.style.height = mapImage.height + "px";
	}
	
	initCanvas(tilesCanvasElement);
	initCanvas(playersCanvasElement);

	playersCanvas.font = "21pt Crimson Text";
	playersCanvas.textAlign = "center";
	playersCanvas.fillStyle = "#e8d255";
	playersCanvas.strokeStyle = "#6d351b";
	playersCanvas.lineWidth = 4;

	drawTiles();
	updateMap();
	readyTurn();
}

// Drawing static graphics, tiles, path
function drawTiles()
{
	let mapResMp = mapImage.width / mapImageSize.x; // Map-element to map source image ratio
	let imgPivotOffset = 15;

	tilesCanvas.clearRect(0, 0, tilesCanvasElement.width, tilesCanvasElement.height);

	tilesCanvas.beginPath();
	tilesCanvas.setLineDash([6, 6]);
	tilesCanvas.strokeStyle = "#6d351b"; // yellow_l
	tilesCanvas.lineWidth = 10;
	for(let step of mapSteps)
		tilesCanvas.lineTo(step.x * mapResMp, step.y * mapResMp);
	tilesCanvas.stroke();

	tilesCanvas.beginPath();
	tilesCanvas.strokeStyle = "#e8e08f"; // yellow_l
	tilesCanvas.lineWidth = 4;
	for(let step of mapSteps)
		tilesCanvas.lineTo(step.x * mapResMp, step.y * mapResMp);
	tilesCanvas.stroke();

	// Drawing regular tiles
	let tileImage = new Image();
	tileImage.onload = function()
	{
		for(let step of mapSteps)
		{
			if (step.trap == undefined)
				tilesCanvas.drawImage(tileImage, 
					(step.x * mapResMp) - imgPivotOffset, 
					(step.y * mapResMp) - imgPivotOffset, 
					30, 30);
		}
	}

	tileImage.src = "_rs/images/step.png";

	// Drawing trap tiles
	let trapImage = new Image();
	trapImage.onload = function()
	{
		for(let step of mapSteps)
		{
			if (step.trap != undefined)
				tilesCanvas.drawImage(trapImage, 
					(step.x * mapResMp) - imgPivotOffset, 
					(step.y * mapResMp) - imgPivotOffset, 
					30, 30);
		}
	}

	trapImage.src = "_rs/images/step-trap.png";

	setInterval(animatePlayer, 50);
}

// Re-drawing the players map pieces based on their positions
function updateMap()
{
	let mapWidth = playersCanvasElement.width;
	let mapHeight = playersCanvasElement.height;
	let mapResMp = mapImage.width / mapImageSize.x; // Map-element to map source image ratio
	let imgPivotOffset = 15;

	let size = {x: 60, y: 100};
	let pivot = {x: 30, y: 100};

	playersCanvas.clearRect(0, 0, playersCanvasElement.width, playersCanvasElement.height);

	for(let card of cards)
	{
		let mapStep = mapSteps[card.stepIndex];

		let pieceImage = new Image();
		pieceImage.onload = function()
		{
			//let position = {x: mapStep.x * mapResMp, y: mapStep.y * mapResMp};
			let position = {x: card.position.x * mapResMp, y: card.position.y * mapResMp};
			playersCanvas.drawImage(pieceImage, 
				position.x - pivot.x, 
				position.y - pivot.y, 
				size.x, size.y);

			playersCanvas.strokeText(card.name, position.x, position.y - 80);
			playersCanvas.fillText(card.name, position.x, position.y - 80);
		}

		pieceImage.src = "_rs/images/map-pieces/"+card.houseNameShort+".png";
	}
}

var playerIsMoving = false;

// Interval-method: calculating movement and re-drawing the map pieces
function animatePlayer()
{
	if (!playerIsMoving) return;

	let moveSpeed = .2;

	// translation
	let targetPos = mapSteps[currentCard.stepIndex];
	let currPos = currentCard.position;
	let newPos = {x: currPos.x + ((targetPos.x - currPos.x) * moveSpeed), 
				  y: currPos.y + ((targetPos.y - currPos.y) * moveSpeed)}

	currentCard.position = newPos;

	// test positions
	let playerDistance = 
		distance(currentCard.position, mapSteps[currentCard.stepIndex]);

	if (playerDistance < 1)
	{
		currentCard.position = targetPos;
		playerIsMoving = false;
		validatePlayer(currentCard);
	}

	updateMap();
}

var minTile = 0;
var maxTile = mapSteps.length - 1;

// Method for moving a player. This will start the movement-animation 
function movePlayer(card, steps)
{
	let newStepIndex = Math.min(Math.max(currentCard.stepIndex + steps, minTile), maxTile);
	card.stepIndex = newStepIndex;
	playerIsMoving = true;
}

// Checking for traps or win when a player has been moved to its target tile
function validatePlayer(card)
{
	let gameFinished = card.stepIndex == maxTile;

	if (gameFinished)
		finishGame(card);

	let trap = (mapSteps[card.stepIndex].trap != undefined)
		? mapSteps[card.stepIndex].trap
		: null;

	if (trap != null)
	{
		movePlayer(card, trap.steps);
		setMessage(card.name + trap.name);
	}

	else
		readyTurn();
}

// Waiting for the player to roll the dice
function readyTurn()
{
	// Bonus turn: not switching player
	if (lastDiceRoll == 6 && canGetBonusTurn)
	{
		canGetBonusTurn = false;
		setMessage("Bonus turn! " + currentCard.name + " gets an extra roll");
	}

	// Regular turn: switching player
	else
	{
		currentCardTurnIndex = currentCardTurnIndex == 0
			? 1 : 0;
		currentCard = cards[currentCardTurnIndex];
		canGetBonusTurn = true;
		setMessage(currentCard.name + "s turn");
	}

	diceRollReady = true;
	diceTurn.innerHTML = "Click to roll";
}

// Called when a player has reached Kings' Landing, and won the gamle
function finishGame(winCard)
{
	let winScreenElement = document.getElementById("winScreen");
	winScreenElement.style.display = "flex";

	winScreenElement.innerHTML = `
		<h1>Protector of the realm</h1>`;
	winScreenElement.appendChild(getCardElement(winCard, false));
	winScreenElement.innerHTML += `
		<a href="index.html">Play again</a>`;
}

// Variables related to the dice
var dicePosition = {x: diceCanvasElement.width / 2, y: diceCanvasElement.height / 2};
var diceDirection = {x: 0, y: 0};
var diceAngle = 0;

var diceSpeed = 0;
var diceDamping = 1;

var diceSize = 60;
var diceUpdate = null;

var diceCanvasWidth = diceCanvasElement.width;
var diceCanvasHeight = diceCanvasElement.height;

// Simply returns a number from 1 to 6
function getDiceRoll()
{
	return Math.floor(Math.random() * 6) + 1;
}

// This will either roll the dice, or accelerate the dice rolling
function onDiceClick()
{
	if (!diceRollReady) return;

	// Start rolling
	if (diceUpdate == null)
		startDiceRoll();

	// Accelerate rolling
	else
		diceSpeed = Math.max(diceSpeed + 5, 80);
}

// Setting initial dice roll physics stats
function startDiceRoll()
{
	diceTurn.innerHTML = "";
	diceSpeed = 40;
	diceDirection = {x: .5, y: .2};
	diceUpdate = setInterval(diceAnimationUpdate, 50);
}

// Calculating speed and torque, and drawing the dice
function diceAnimationUpdate()
{
	let currentRoll = getDiceRoll();
	let diceImage = new Image();

	diceImage.onload = function()
	{
		diceCanvas.clearRect
			(0, 0, diceCanvasWidth, diceCanvasHeight);

		let fixedPosition = {x: dicePosition.x - (diceSize / 2), y: dicePosition.y - (diceSize / 2)};

		// Setting dice position and rotation
		diceCanvas.translate(fixedPosition.x, fixedPosition.y);
		diceCanvas.rotate(diceAngle*Math.PI/180);
		diceCanvas.drawImage
			(diceImage, -diceSize / 2, -diceSize / 2, diceSize, diceSize);
		diceCanvas.rotate(-diceAngle*Math.PI/180);
		diceCanvas.translate(-fixedPosition.x, -fixedPosition.y);

		// Updating dice torque
		let diceTorque = Math.min(diceSpeed, 40);
		diceAngle += diceTorque;
		if (diceAngle > 360) diceAngle -= 360;

		// Updating dice speed
		dicePosition.x = Math.min(Math.max(
			0, dicePosition.x + diceDirection.x * diceSpeed), diceCanvasWidth);
		dicePosition.y = Math.min(Math.max(
			0, dicePosition.y + diceDirection.y * diceSpeed), diceCanvasHeight);

		let boundPadding = diceSize * .3;

		// Dice bounding box
		if (dicePosition.x >= diceCanvasWidth - boundPadding)
			diceDirection.x *= -1;

		if (dicePosition.x <= diceSize + boundPadding)
			diceDirection.x *= -1;

		if (dicePosition.y >= diceCanvasHeight - boundPadding)
			diceDirection.y *= -1;

		if (dicePosition.y <= diceSize + boundPadding)
			diceDirection.y *= -1;

		diceSpeed -= diceDamping;

		// Checking if dice has finished rolling
		if (diceSpeed <= 0)
		{
			clearInterval(diceUpdate);
			diceUpdate = null;
			diceSpeed = 0;

			lastDiceRoll = currentRoll;
			diceRollReady = false;
			setMessage(currentCard.name + " rolled " + currentRoll);
			movePlayer(currentCard, currentRoll);
		}
	}

	diceImage.src = `_rs/images/dice/`+currentRoll+`.png`;

}

// Method for setting the game message, like current player, trap message, bonus roll, etc
function setMessage(message)
{
	popupElement.getElementsByClassName("popup")[0].innerHTML = message;
	console.log(message);
}