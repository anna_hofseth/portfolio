<?php 
    $success = false;
    $to = "anna.e.hofseth@gmail.com";

    $fromMail = "";
    $fromName = "";
    $title = "";
    $message = "";

    $fullMessage  = "";

    if ($_GET) {
        foreach ($_GET as $key => $value) {
            $fullMessage .= "\t"."$key: $value"."\r\n";

            if($key == "navn") {
                $fromName = $value; 
            }
            if($key == "email") {
                $fromMail = $value; 
            }
            if($key == "tema") {
                $title = $value; 
            }
            if($key == "melding") {
                $message = $value; 
            }
        }
    } elseif ($_POST) {
         foreach ($_POST as $key => $value) {
            $fullMessage .= "\t"."Hemmelig $key: $value"."\r\n";
            if ($key == "email") {
                $fromMail = $value; 
            }
         }
    } 

    if ($fullMessage) {
        if (mail($to, "$title (via hovlia.no)", "Fra $fromName\n$fromMail \n\n $message", "From: $fromMail")
        )
        {
            $success = true; 
        }
    }
?>
<!DOCTYPE html>
<html lang="no">
    <?php if ($success): ?>
    <script>
        let message = 
        `{"name":"<?= $fromName ?>","email":"<?= $fromMail ?>","title":"<?= $title ?>","message":"<?= $message ?>"}`;
        localStorage.setItem("hovlia_sent-message", message);
    </script>
    <?php 
        else:
    ?>
    <script>
        localStorage.removeItem("hovlia_sent-message");
    </script>
    <?php
        endif;
    ?>
    <script>
        location.assign("http://annahofseth.com/noroff/hovlia/#/posted/");
    </script>
  </body>
</html>