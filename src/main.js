import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'

import About from './pages/About.vue'
import Contact from './pages/Contact.vue'
import Resume from './pages/Resume.vue'
import Showcase from './pages/Showcase.vue'
import Project from './pages/Project.vue'
import NotFound from './pages/404.vue'
Vue.use(VueRouter)

Vue.config.productionTip = false

const router = new VueRouter({
  routes: [
    {
      path: "/",
      name: "Home",
      component: Showcase
    },
    {
      path: "/about",
      name: "About",
      component: About
    },
    {
      path: "/contact",
      name: "Contact",
      component: Contact
    },
    {
      path: "/resume",
      name: "Resume",
      component: Resume
    },
    {
      path: "/showcase/:tags?",
      name: "Showcase",
      component: Showcase,
      props: true
    },
    {
      path: "/project/:id",
      name: "Project",
      component: Project,
      props: true
    },
    {
      path: "*",
      component: NotFound
    }
  ]
});

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
